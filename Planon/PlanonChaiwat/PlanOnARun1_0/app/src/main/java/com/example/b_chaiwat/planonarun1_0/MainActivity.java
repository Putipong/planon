package com.example.b_chaiwat.planonarun1_0;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;


public class MainActivity extends Activity {

    LocalActivityManager mLocalActivityManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLocalActivityManager = new LocalActivityManager(this, false);
        mLocalActivityManager.dispatchCreate(savedInstanceState);

        TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup(mLocalActivityManager);

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("To Do List")
                .setIndicator("To Do List")
                .setContent(new Intent(this, To_Do_List.class));
        TabHost.TabSpec tabSpec2 = tabHost.newTabSpec("Calendar")
                .setIndicator("Calendar")
                .setContent(new Intent(this, Calendar.class));
        TabHost.TabSpec tabSpec3 = tabHost.newTabSpec("Goal")
                .setIndicator("Goal")
                .setContent(new Intent(this, Goal.class));

        tabHost.addTab(tabSpec);
        tabHost.addTab(tabSpec2);
        tabHost.addTab(tabSpec3);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocalActivityManager.dispatchPause(!isFinishing());

    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocalActivityManager.dispatchResume();
    }
}