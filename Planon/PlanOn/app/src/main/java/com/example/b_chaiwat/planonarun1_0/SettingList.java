package com.example.b_chaiwat.planonarun1_0;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.b_chaiwat.planonarun1_0.data.DatabaseHelper;

import java.util.Calendar;


public class SettingList extends ActionBarActivity {

    private Button button_on_date, button_save,button_time,button_set_dateline;
    private String set_time;
    private int pHour,pMinute,pDate,pMonth,pYear;
    private int sDate ,sMonth ,sYear ;
    private int eDate ,eMonth ,eYear ;
    private int fHour,fMinute;
    private EditText edTitle,edDuration,edDetail;
    protected static final  int TIME_DIALOG_ID = 0;
    protected static final  int DATE_DIALOG_ID_START = 1;
    protected static final  int DATE_DIALOG_ID_END = 2;

    private String rowKey;
    Integer rowID;

    public ArrayAdapter<String> adapter;


    DatabaseHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_list);


        //Intent intent = this.getIntent();
        edTitle=(EditText) findViewById(R.id.edit_list_title);
        edDuration=(EditText) findViewById(R.id.editText);
        edDetail=(EditText) findViewById(R.id.edit_list_detail);

        /** Get the current time */
        final java.util.Calendar cal = java.util.Calendar.getInstance();
        pHour = cal.get(Calendar.HOUR_OF_DAY);
        pMinute = cal.get(Calendar.MINUTE);
        pYear = cal.get(Calendar.YEAR);
        pMonth = cal.get(Calendar.MONTH)+1;
        pDate = cal.get(Calendar.DAY_OF_MONTH);

        //get first data
        sYear = cal.get(Calendar.YEAR);
        sMonth = cal.get(Calendar.MONTH)+1;
        sDate = cal.get(Calendar.DAY_OF_MONTH);
        //
        eYear = cal.get(Calendar.YEAR);
        eMonth = cal.get(Calendar.MONTH)+1;
        eDate = cal.get(Calendar.DAY_OF_MONTH);
        //
        fHour = cal.get(Calendar.HOUR_OF_DAY);
        fMinute = cal.get(Calendar.MINUTE);

        button_on_date = (Button) findViewById(R.id.button_set_date);
        button_set_dateline = (Button) findViewById(R.id.button_set_deadline);

        set_time = getIntent().getStringExtra("text_date");
        button_on_date.setText(set_time);
        button_on_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID_START);
            }
        });

        button_time = (Button) findViewById(R.id.button_times);
        button_time.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(TIME_DIALOG_ID);
            }
        });

        button_set_dateline.setText("select dead line");
        button_set_dateline.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID_END);

            }
        });

        button_save = (Button) findViewById(R.id.button_save);
        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
                finish();
                /*if (edTitle.length() != 0){
                    if (sDate == eDate && sMonth == eMonth && sYear == eYear){//--case1
                        if ((fHour == pHour && fMinute != pMinute && fMinute < pMinute)||(fHour != pHour && fHour < pHour)){
                            saveData();
                            finish();
                            Toast.makeText(getApplicationContext(), "Saved case1 " + sDate + "/" + sMonth + "/" + sYear + " to " + eDate + "/" + eMonth + "/" + eYear + ".", Toast.LENGTH_LONG).show();//check data
                        } else {
                            Toast.makeText(getApplicationContext(), "Please change time more then 'Real time'.", Toast.LENGTH_LONG).show();//check time on same date
                        }
                    } else if (sYear != eYear && sYear < eYear){//--case2
                        saveData();
                        finish();
                        Toast.makeText(getApplicationContext(), "Saved case2 " + sDate + "/" + sMonth + "/" + sYear + " to " + eDate + "/" + eMonth + "/" + eYear + ".", Toast.LENGTH_LONG).show();//check data
                    } else if (sYear == eYear && sMonth != eMonth && sMonth < eMonth) {//--case3
                        saveData();
                        finish();
                        Toast.makeText(getApplicationContext(), "Saved case3 " + sDate + "/" + sMonth + "/" + sYear + " to " + eDate + "/" + eMonth + "/" + eYear + ".", Toast.LENGTH_LONG).show();//check data
                    }else if (sYear == eYear && sMonth == eMonth && sDate != eDate && sDate < eDate) {//--case4
                        saveData();
                        finish();
                        Toast.makeText(getApplicationContext(), "Saved case4 " + sDate + "/" + sMonth + "/" + sYear + " to " + eDate + "/" + eMonth + "/" + eYear + ".", Toast.LENGTH_LONG).show();//check data
                    }else {
                        Toast.makeText(getApplicationContext(), "Please edit your data, 'Start data' must less then 'End data'.", Toast.LENGTH_LONG).show();
                    }


                }
                else {
                    Toast.makeText(getApplicationContext(), "Please input Title name.", Toast.LENGTH_LONG).show();

                }*/

            }
        });
        Intent intent = this.getIntent();
        if (intent != null && intent.hasExtra(Intent.EXTRA_TEXT)) {
            rowKey = intent.getStringExtra(Intent.EXTRA_TEXT);
            rowID = Integer.parseInt(rowKey)-1;
            edTitle.setText(getTitle(rowID));
            button_on_date.setText(getSTime(rowID));
            button_set_dateline.setText(getETime(rowID));
            //button_time.setText(getDLTime(rowID));
            edDuration.setText(getDu(rowID));
            edDetail.setText(getDetail(rowID));
        }

        button_time.setText(formatTime(pHour)+":"+formatTime(pMinute));
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_setting_list, menu);
        return true;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.home){
            finish();
            //Do something before
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                return new TimePickerDialog(this,
                        mTimeSetListener, pHour, pMinute, false);
            case DATE_DIALOG_ID_START:
                return new DatePickerDialog(this,mDateSetStartListener,pYear,pMonth,pDate);
            case DATE_DIALOG_ID_END:

                return new DatePickerDialog(this,mDateSetEndListener,pYear,pMonth,pDate);
        }

        return null;
    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    pHour = hourOfDay;
                    pMinute = minute;
                    button_time.setText(formatTime(pHour)+":"+formatTime(pMinute));
                }
            };

    private DatePickerDialog.OnDateSetListener mDateSetStartListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear ,int dayOfMonth) {
                    pDate = dayOfMonth;
                    pMonth = monthOfYear+1;
                    pYear = year;

                    sDate = pDate;
                    sMonth = pMonth;
                    sYear = pYear;

                    button_on_date.setText(pDate+"/"+pMonth+"/"+pYear);
                }

            };
    private DatePickerDialog.OnDateSetListener mDateSetEndListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear ,int dayOfMonth) {
                    pDate = dayOfMonth;
                    pMonth = monthOfYear+1;
                    pYear = year;

                    eDate = pDate;
                    eMonth = pMonth;
                    eYear = pYear;

                    button_set_dateline.setText(pDate+"/"+pMonth+"/"+pYear);
                }

            };
    private String formatTime(int time){
        String str=time+"";
        if(str.length()==1){
            str="0"+str;
        }
        return str;
    }

    private void saveData(){
        dbHelper = new DatabaseHelper(this);
        String title = edTitle.getText().toString();
        String date = button_on_date.getText().toString();
        String dlDate = button_set_dateline.getText().toString();
        String duration = edDuration.getText().toString();
        String detail = edDetail.getText().toString();
        dbHelper.insertData(title, date, dlDate, duration, detail);
        dbHelper.close();
    }

    private String getTitle(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_TITLE,row);
        dbHelper.close();
        return value;
    }
    private String getDetail(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_DETAIL,row);
        dbHelper.close();
        return value;
    }
    private String getID(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_KEY,row);
        dbHelper.close();
        return value;
    }
    private String getETime(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_DL_DATE,row);
        dbHelper.close();
        return value;
    }

    private String getSTime(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_DATE,row);
        dbHelper.close();
        return value;
    }
    private String getDLTime(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_TimeLeft,row);
        dbHelper.close();
        return value;
    }
    private String getDu(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_DURATION,row);
        dbHelper.close();
        return value;
    }

}
