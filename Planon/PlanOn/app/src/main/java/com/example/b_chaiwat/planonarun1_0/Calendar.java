package com.example.b_chaiwat.planonarun1_0;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

/**
 * Created by b_chaiwat on 5/3/2558.
 */

public class Calendar extends Activity {
    private String text_date;
    private Button button_click_add;

    private int pDay;
    private int pMonth;
    private int pYear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_carlendar);
        button_click_add = (Button) findViewById(R.id.button_ADD);

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.linear);//เหมือนกับในtab_carlendar.xml
        relativeLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
        CalendarView calendarview = (CalendarView) findViewById(R.id.calendarView);
        final java.util.Calendar cal = java.util.Calendar.getInstance();
        pDay = cal.get(java.util.Calendar.DAY_OF_MONTH);//got this code in real time (Fn Calendar)
        pMonth = cal.get(java.util.Calendar.MONTH);
        pYear = cal.get(java.util.Calendar.YEAR);
        calendarview.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                month+=1;
                text_date = Integer.toString(dayOfMonth) + "/" + month + "/" + Integer.toString(year); //got data month day year //got data month from Fn getMonthString
                Toast.makeText(getApplicationContext(),text_date,Toast.LENGTH_LONG).show();//test
                MainActivity.setDateText(text_date);

            }
        });

        button_click_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text_date == null) {
                    pMonth+=1;
                    text_date = Integer.toString(pDay) + "/" + pMonth + "/" + Integer.toString(pYear) ;
                }
                Intent i = new Intent(getApplicationContext(), SettingList.class);
                i.putExtra("text_date", text_date);
                startActivity(i);
            }
        });
    }
    public String getMonthString(int month){
        String month_string=null;
        month = month+1;//month start at "0"
        if (month == 1){month_string = "JANUARY";}
        else if (month == 2) {month_string = "FEBRUARY";}
        else if (month == 3) {month_string = "MARCH";}
        else if (month == 4) {month_string = "APRIL";}
        else if (month == 5) {month_string = "MAY";}
        else if (month == 6) {month_string = "JUNE";}
        else if (month == 7) {month_string = "JULY";}
        else if (month == 8) {month_string = "AUGUST";}
        else if (month == 9) {month_string = "SEPTEMBER";}
        else if (month == 10) {month_string = "OCTOBER";}
        else if (month == 11) {month_string = "NOVEMBER";}
        else if (month == 12) {month_string = "DECEMBER";}
        return month_string;
    }
}