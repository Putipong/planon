package com.example.b_chaiwat.planonarun1_0;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;

import android.os.Bundle;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.b_chaiwat.planonarun1_0.data.DatabaseHelper;

import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * Created by b_chaiwat on 5/3/2558.
 */
public class To_Do_List extends Activity {
    private ArrayAdapter<String> mForecastAdapter;
    TextView testN;
    DatabaseHelper dbHelper;

    final long startCountDown = 30 * 1000;
    final long intervalCountDown = 1 * 1000;

    public ArrayAdapter<String> adapter;
    private ListView listTodo;
    private ArrayList<String> listingArr;

    private String listID;
    private Button button_left,button_right;
    private TextView textView_date;
    private int pDate,pMonth,pYear;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_to_do_list);

        /*testN = (TextView) findViewById(R.id.TestNumber1);
        Button buttonT = (Button) findViewById(R.id.buttonT);
        buttonT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testN.setText(getTimeEnd());
            }
        });*/

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linear);
        linearLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));//set bg color

        listTodo = (ListView) findViewById(R.id.listView2);
        registerForContextMenu(listTodo);
        listingArr = new ArrayList<String>();

        adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,listingArr);
        listTodo.setAdapter(adapter);

        listTodo.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listTodo.setBackgroundColor(Color.GRAY);


        dbHelper = new DatabaseHelper(this);
        if(!dbHelper.isEmpty()){
            for(int i=0;i<=getLastRow()-1;i++) {
                String mes = getID(i);
                String mes2 = getTitle(i);
                //listingArr.add(getTitle(i));
                listingArr.add(mes+": " + mes2);
            }
        };

         /*for (int i=1;i<9;i++) {
            tmpString = null;
            tmpString = getPreferences(MODE_PRIVATE).getString("Spiel"+i,"");

            if (tmpString.equals("yes")) {
                // -----> MISSING LINE HERE <-----
                Log.i(tag, "Spiel" + i + "-value is YES!");
            }

        }*/

        button_left = (Button) findViewById(R.id.button_shift_left);
        button_right = (Button) findViewById(R.id.button_shift_right);
        Button rfbt = (Button) findViewById(R.id.button);

        rfbt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                refresh();
            }
        });
        textView_date = (TextView) findViewById(R.id.textView_G);
        //get current time
        final java.util.Calendar cal = java.util.Calendar.getInstance();
        pYear = cal.get(java.util.Calendar.YEAR);
        pMonth = cal.get(java.util.Calendar.MONTH)+1;
        pDate = cal.get(java.util.Calendar.DAY_OF_MONTH);
        textView_date.setText(gotStringMonth(pMonth) + " " + Integer.toString(pDate) + " " + Integer.toString(pYear));

        button_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDate = pDate-1;
                if(pDate == 0){
                    if (pMonth==2||pMonth==4||pMonth==6||pMonth==8||pMonth==9||pMonth==11||pMonth==1){//เดือนที่มี31วัน
                        pMonth = pMonth-1;
                        pDate = 31;
                        if(pMonth == 0){
                            pMonth = 12;
                            pYear = pYear-1;
                            if (pYear == 0){
                                pYear = 0;
                            }
                        }
                    }else if (pMonth==5||pMonth==7||pMonth==10||pMonth==12){//เดือนที่มี30
                        pMonth = pMonth-1;
                        pDate = 30;
                    }else if (pMonth==3){//เฉพาะ 2
                        if(pYear%4!=0){
                            pMonth = pMonth-1;
                            pDate = 29;
                        }else if(pYear%4==0){
                            pMonth = pMonth-1;
                            pDate = 28;
                        }
                    }
                }
                textView_date.setText(gotStringMonth(pMonth) + " " + Integer.toString(pDate) + " " + Integer.toString(pYear));
            }


        });

        button_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDate = pDate+1;
                if (pMonth==1||pMonth==3||pMonth==5||pMonth==7||pMonth==8||pMonth==10||pMonth==12){//เดือนที่มี31วัน
                    if (pDate==32){
                        pDate = 1;
                        pMonth = pMonth+1;
                        if (pMonth == 13){
                            pMonth = 1;
                            pYear = pYear+1;
                        }
                    }
                }else if (pMonth==4||pMonth==6||pMonth==9||pMonth==11) {//เดือนที่มี30
                    if (pDate==31){
                        pDate = 1;
                        pMonth = pMonth+1;
                        if (pMonth == 13){
                            pMonth = 1;
                            pYear = pYear+1;
                        }
                    }
                }else if (pMonth==2){//เฉพาะเดือน2
                    if(pYear%4!=0){
                        if (pDate==30){
                            pDate = 1;
                            pMonth = pMonth+1;
                            if (pMonth == 13){
                                pMonth = 1;
                                pYear = pYear+1;
                            }
                        }
                    }else if(pYear%4==0){
                        if (pDate==29){
                            pDate = 1;
                            pMonth = pMonth+1;
                            if (pMonth == 13){
                                pMonth = 1;
                                pYear = pYear+1;
                            }
                        }
                    }
                }
                textView_date.setText(gotStringMonth(pMonth) + " " + Integer.toString(pDate) + " " + Integer.toString(pYear));
            }
        });
    }


    private String gotStringMonth(int month) {
        String month_string=null;
        month = month;//month start at "0"
        if (month == 1){month_string = "JANUARY";}
        else if (month == 2) {month_string = "FEBRUARY";}
        else if (month == 3) {month_string = "MARCH";}
        else if (month == 4) {month_string = "APRIL";}
        else if (month == 5) {month_string = "MAY";}
        else if (month == 6) {month_string = "JUNE";}
        else if (month == 7) {month_string = "JULY";}
        else if (month == 8) {month_string = "AUGUST";}
        else if (month == 9) {month_string = "SEPTEMBER";}
        else if (month == 10) {month_string = "OCTOBER";}
        else if (month == 11) {month_string = "NOVEMBER";}
        else if (month == 12) {month_string = "DECEMBER";}
        return month_string;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
        String obj = (String) listTodo.getItemAtPosition(acmi.position);
        String[] separated = obj.split(":");
        listID = separated[0];
        //Integer obj = (Integer) listTodo.getPositionForView(v);
        menu.add(0, v.getId(), 0, "Start");
        menu.add(0, v.getId(), 1, "Detail");//groupId, itemId, order, title
        menu.add(0, v.getId(), 2, "Edit");
        menu.add(0, v.getId(), 3, "Delete");
        //menu.add(0, v.getId(), 2, separated[0]);
        //menu.add(0, v.getId(), 2, obj);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        if(item.getTitle()=="Start"){
            dbHelper = new DatabaseHelper(this);
            Integer rowID = Integer.parseInt(listID);
            int startTime = Integer.parseInt(dbHelper.getData(DatabaseHelper.COLUMN_TimeLeft, rowID - 1));
            //MyCountDownTimer clockRe = MyCountDownTimer(startTime*1000*60*60,intervalCountDown);
            //Todo: get clock running
            MyCountDownTimer clock = new MyCountDownTimer(startTime*1000*60*60,intervalCountDown);
            if(!clock.running){
            clock.start();
                clock.running = true;
            }else if(clock.running){
                //Todo Update database
                //clock.timeLeft;
                clock.cancel();
            }


            Toast.makeText(getApplicationContext(),"Start",Toast.LENGTH_LONG).show();
        }
        if(item.getTitle()=="Detail"){
            //Toast.makeText(getApplicationContext(), "detail", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(this, Detail.class)
                    .putExtra(Intent.EXTRA_TEXT,listID);
            startActivity(intent);
        }
        else if(item.getTitle()=="Edit"){
            Intent intent = new Intent(this, SettingList.class)
                    .putExtra(Intent.EXTRA_TEXT,listID);
            startActivity(intent);
        }
        else if(item.getTitle()=="Delete"){
            Integer rowID = Integer.parseInt(listID);
            dbHelper.DeleteData(rowID);
            refresh();

        }
        else{
            return false;
        }
        return true;
    }


    private String getTitle(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_TITLE,row);
        dbHelper.close();
        return value;
    }
    private String getID(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_KEY,row);
        dbHelper.close();
        return value;
    }
    private int getLastRow(){
        dbHelper = new DatabaseHelper(this);
        int value = dbHelper.getLastRow();
        dbHelper.close();
        return value;
    }

    public void refresh(){
        adapter.clear();

        dbHelper = new DatabaseHelper(this);
        if(!dbHelper.isEmpty()){
            for(int i=0;i<=getLastRow()-1;i++) {
                String mes = getID(i);
                String mes2 = getTitle(i);
                //listingArr.add(getTitle(i));
                listingArr.add(mes+": " + mes2);
            }
        };
    }


    public class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }
        boolean running;
        long timeLeft;

        @Override
        public void onFinish() { //when finish

        }

        @Override
        public void onTick(long millisUntilFinished) {
            timeLeft = millisUntilFinished/1000;

            Log.e("Tick",String.format("%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            if(millisUntilFinished%3600000 == 0){
                //Todo take to database;
            }
        }
    }
}