package com.example.b_chaiwat.planonarun1_0;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
//<<<<<<< HEAD
import android.support.v7.app.ActionBarActivity;
//=======
import android.view.Menu;
//<<<<<<< HEAD
//>>>>>>> 44d430d182ea470866d9a75097326553bf99bb52
//=======
import android.view.MenuItem;
//>>>>>>> 9b96296748203cdfb942a07eb5df7913b307b064
import android.widget.TabHost;
import com.example.b_chaiwat.planonarun1_0.data.DatabaseHelper;


public class MainActivity extends ActionBarActivity {

    private static String dateText = "History";
    DatabaseHelper db;

    LocalActivityManager mLocalActivityManager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLocalActivityManager = new LocalActivityManager(this, false);
        mLocalActivityManager.dispatchCreate(savedInstanceState);

        db = new DatabaseHelper(this);
        db.getWritableDatabase();

        TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup(mLocalActivityManager);

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("To Do List")
                .setIndicator("To Do List")
                .setContent(new Intent(this, To_Do_List.class));
        TabHost.TabSpec tabSpec2 = tabHost.newTabSpec("Calendar")
                .setIndicator("Calendar")
                .setContent(new Intent(this, Calendar.class));
        TabHost.TabSpec tabSpec3 = tabHost.newTabSpec("History")
                .setIndicator("History")
                .setContent(new Intent(this, Goal.class));

        tabHost.addTab(tabSpec);
        tabHost.addTab(tabSpec2);
        tabHost.addTab(tabSpec3);

         //   getSupportFragmentManager().beginTransaction()
           //         .add(R.id.container, new To_Do_List())//ไปยังไฟล์นี้
             //       .commit();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mLocalActivityManager.dispatchPause(!isFinishing());

    }

    @Override
    protected void onResume() {
        super.onResume();
        mLocalActivityManager.dispatchResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
   public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.action_settings){
            Intent intentSet = new Intent(this,Setting.class);
            startActivity(intentSet);
            return true;
        }

        return super.onOptionsItemSelected(item);
   }

    public static String getDateText(){
        return dateText;
    }
    public static void setDateText(String newDate){
        dateText = newDate;
    }

}
