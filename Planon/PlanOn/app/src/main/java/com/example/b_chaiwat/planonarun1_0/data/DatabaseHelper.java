package com.example.b_chaiwat.planonarun1_0.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

    private final String LOG_TAG = DatabaseHelper.class.getSimpleName();
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 4;
    public static final String DATABASE_NAME = "todo.db";
    public static final String TABLE_NAME = "todo";
    public static final String COLUMN_KEY = "KEY";
    public static final String COLUMN_TITLE = "TITLE";
    public static final String COLUMN_DATE = "DATE";
    public static final String COLUMN_DL_DATE = "DL_DATE";
    public static final String COLUMN_TimeLeft = "TimeLeft";
    public static final String COLUMN_DURATION = "DURATION";
    public static final String COLUMN_DETAIL = "DETAIL";
    SQLiteDatabase db;


    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME +
                " (" + COLUMN_KEY + " INTEGER PRIMARY KEY, " +
                COLUMN_TITLE + " TEXT, " +
                COLUMN_DATE + " TEXT, " +
                COLUMN_DL_DATE + " TEXT, " +
                COLUMN_TimeLeft + " TEXT, " +
                COLUMN_DURATION + " TEXT, " +
                COLUMN_DETAIL + " TEXT);");

        Log.d("CREATE TABLE", "Create Table Successfully.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long insertData(String title ,String date,String dlDate,String duration,String detail) {
        db = getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put(COLUMN_TITLE, title);
        content.put(COLUMN_DATE, date);
        content.put(COLUMN_DL_DATE, dlDate);
        content.put(COLUMN_TimeLeft, duration);
        content.put(COLUMN_DURATION, duration);
        content.put(COLUMN_DETAIL, detail);
        long rowNumber = db.insert(TABLE_NAME, null, content);
        db.close();
        return rowNumber;
    }
    public String getTitle(int rowID) {
        db = getReadableDatabase();
        String[] projection = {
                COLUMN_KEY,
                COLUMN_TITLE,
        };
        Cursor c = db.query(                            //SELECT * from tablename where columnname = value ORDER BY column_name ASC|DESC, column_name ASC|DESC;
                TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause(search value in column)
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order(order by)
        );
        //c.moveToFirst();
        c.moveToPosition(rowID);
        String value = c.getString(c.getColumnIndex(COLUMN_TITLE));
        /*if(){

        }*/
        //int columnIndex;
        //columnIndex=c.getColumnIndex(COLUMN_TimeLeft);
        db.close();
        return value;
    }

    public String getData(String column, int rowId){
        String value;
        db = getReadableDatabase();
        String[] projection = {
                COLUMN_KEY,column
        };
        Cursor c = db.query(                            //SELECT * from tablename where columnname = value ORDER BY column_name ASC|DESC, column_name ASC|DESC;
                TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause(search value in column)
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order(order by)
        );
        c.moveToPosition(rowId);
        value = c.getString(c.getColumnIndex(column));
        db.close();
        return value;
    }
    public String getDataFrom(String column, int rowId,String search,String columnSearch){
        String value;
        String[] array = {search};
        db = getReadableDatabase();
        String[] projection = {
                COLUMN_KEY,column,columnSearch
        };
        Cursor c = db.query(                            //SELECT * from tablename where columnname = value ORDER BY column_name ASC|DESC, column_name ASC|DESC;
                TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                columnSearch,                                // The columns for the WHERE clause
                array,                            // The values for the WHERE clause(search value in column)
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order(order by)
        );
        c.moveToPosition(rowId);
        value = c.getString(c.getColumnIndex(column));
        db.close();
        return value;
    }

    public int getLastRow(){
        db = getReadableDatabase();
        String[] projection = {
                COLUMN_KEY
        };
        Cursor c = db.query(                            //SELECT * from tablename where columnname = value ORDER BY column_name ASC|DESC, column_name ASC|DESC;
                TABLE_NAME,                             // The table to query
                projection,                             // The columns to return
                null,                                   // The columns for the WHERE clause
                null,                                   // The values for the WHERE clause(search value in column)
                null,                                   // don't group the rows
                null,                                   // don't filter by row groups
                null                                    // The sort order(order by)
        );
        c.moveToLast();
        //Log.e("Test", c.getColumnName(0)); //get key
        return c.getInt(0);
    }

    public boolean isEmpty(){
        boolean result;
            db = getReadableDatabase();
        String[] projection = {
                COLUMN_KEY
        };
        Cursor c = db.query(                            //SELECT * from tablename where columnname = value ORDER BY column_name ASC|DESC, column_name ASC|DESC;
                TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause(search value in column)
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order(order by)
        );

        boolean test = c.moveToFirst();
        if(test == false){
            result = true;
        }
        else if(test == true){
            result = false;
        }

        else {
            Log.e("Putipong:", "Something went wrong");
            result = true;
        }
        db.close();
        return result;
    }

    public void removeAll()
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void DeleteData(int contact) {


            SQLiteDatabase db;
            db = this.getWritableDatabase(); // Write Data

            //long rows = db.delete(TABLE_NAME, "ID = ?", new String[]{String.valueOf(strMemberID)});
            db.execSQL("DELETE FROM "+TABLE_NAME+" WHERE "+COLUMN_KEY+"='"+contact+"'");
            db.execSQL("UPDATE "+TABLE_NAME+" set "+ COLUMN_KEY +" = ("+COLUMN_KEY+" - 1) WHERE "+COLUMN_KEY+" > "+contact);
            db.close();

        /* catch (Exception e) {
            return -1;
        }*/
    }
}