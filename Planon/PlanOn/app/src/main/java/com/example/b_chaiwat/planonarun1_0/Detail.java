package com.example.b_chaiwat.planonarun1_0;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.b_chaiwat.planonarun1_0.data.DatabaseHelper;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;


public class Detail extends ActionBarActivity {

    private String rowKey;
    Integer rowID;
    DatabaseHelper dbHelper = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Intent intent = getIntent();
        //String message = intent.getStringExtra(To_Do_List.EXTRA_MESSAGE);

        Intent intent = this.getIntent();
        if (intent != null && intent.hasExtra(Intent.EXTRA_TEXT)) {
            rowKey = intent.getStringExtra(Intent.EXTRA_TEXT);
            rowID = Integer.parseInt(rowKey)-1;
            //((TextView) rootView.findViewById(R.id.detail_text))
            //        .setText(mForecastStr);
            TextView tvTitle = (TextView)findViewById(R.id.textView_G);
            TextView tvDetail = (TextView)findViewById(R.id.textView2);
            TextView tvTL = (TextView)findViewById(R.id.textView8);
            String mesTL = getTLTime(rowID);
            String mesDu = getDuTime(rowID);
            tvTitle.setText(getTitle(rowID));
            tvDetail.setText(getDetail(rowID));
            tvTL.setText(mesTL+"Hour(s)");
            TextView tvDoTi = (TextView)findViewById(R.id.done_time);
            tvDoTi.setText(""+(Integer.parseInt(mesDu)-Integer.parseInt(mesTL))+"Hour(s)");
        }


        GraphView graph = (GraphView) findViewById(R.id.graph);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, 1),
                new DataPoint(1, 5),
                new DataPoint(2, 3),
                new DataPoint(3, 2),
                new DataPoint(4, 6)
        });
        series.setColor(Color.CYAN);
        graph.addSeries(series);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String getTitle(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_TITLE, row);
        dbHelper.close();
        return value;
    }

    private String getDetail(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_DETAIL,row);
        dbHelper.close();
        return value;
    }
    private String getTLTime(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_TimeLeft,row);
        dbHelper.close();
        return value;
    }
    private String getDuTime(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_DURATION,row);
        dbHelper.close();
        return value;
    }
}
