package com.example.b_chaiwat.planonarun1_0;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.b_chaiwat.planonarun1_0.data.DatabaseHelper;

import java.util.ArrayList;

/**
 * Created by b_chaiwat on 5/3/2558.
 */
public class Goal extends Activity {
    private TextView tv_goal;
    private String text_date;
    private ArrayAdapter<String> adapter;
    private ListView listGoal;
    private ArrayList<String> listingArr;
    DatabaseHelper dbHelper;

    private String tmpString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_goal);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linear);
        linearLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));

        Intent intent = getIntent();
        if(intent != null && intent.hasExtra("text_datena")){
            tv_goal = (TextView) findViewById(R.id.textView_G);
            text_date = getIntent().getStringExtra("text_datena");
            tv_goal.setText(text_date);
        }
        tv_goal = (TextView) findViewById(R.id.textView_G);
        tv_goal.setText(MainActivity.getDateText());
        //text_date = getIntent().getExtras().getString("text_date");

        Button refreshBT = (Button) findViewById(R.id.button2);
        refreshBT.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                refresh();
            }
        });

        listGoal = (ListView) findViewById(R.id.listViewGoal);
        listingArr = new ArrayList<String>();

        adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.simple_textview,listingArr);
        listGoal.setAdapter(adapter);
        listGoal.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        listingArr.add("test1\n   DeadLine: xx/xx/xxxx\n   TimeLeft: xx hour xx min");
        listingArr.add("test2\n   DeadLine: xx/xx/xxxx\n   TimeLeft: xx hour xx min");
        updateDate();

    }
    private String getTitle(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_TITLE,row);
        dbHelper.close();
        return value;
    }
    private String getID(int row){
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_KEY,row);
        dbHelper.close();
        return value;
    }
    private int getLastRow(){
        dbHelper = new DatabaseHelper(this);
        int value = dbHelper.getLastRow();
        dbHelper.close();
        return value;
    }
    private void updateDate() {
        if (!listingArr.isEmpty()){
            listingArr.clear();
            dbHelper = new DatabaseHelper(this);
            if(!dbHelper.isEmpty()){
                for(int i=0;i<=getLastRow()-1;i++) {
                    String mes = getID(i);
                    String mes2 = getTitle(i);
                    String mesTStart = getSTime(i);
                    String mesTEnd = getETime(i);
                    String mesTL = getTLTime(i);
                    listingArr.add(mes+": " + mes2 + "\n   Start : " + mesTStart + "\n"+"   Stop : " + mesTEnd +"\nTime Left : " + mesTL );
                }
            };
        }
    }

    public void refresh(){
        adapter.clear();

        if(!dbHelper.isEmpty()){
            for(int i=0;i<=getLastRow()-1;i++) {
                String mes = getID(i);
                String mes2 = getTitle(i);
                String mesTStart = getSTime(i);
                String mesTEnd = getETime(i);
                String mesTL = getTLTime(i);
                listingArr.add(mes+": " + mes2 + "\n   Start : " + mesTStart + "\n"+"   Finish : " + mesTEnd +"\n   Time Left : " + mesTL );
            }
        };
    }

    private String getETime(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_DL_DATE,row);
        dbHelper.close();
        return value;
    }

    private String getTLTime(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_TimeLeft,row);
        dbHelper.close();
        return value;
    }

    private String getSTime(int row) {
        dbHelper = new DatabaseHelper(this);
        String value = dbHelper.getData(DatabaseHelper.COLUMN_DATE,row);
        dbHelper.close();
        return value;
    }
//Todo: listener for changing date on the second time.
//Todo: Idea:try broadcast intent
}